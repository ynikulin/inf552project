#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace cv;
using namespace std;

#pragma region CONSTS
	static const int SEAM_KERNEL = 1;
	static const int SOBEL_KERNEL = 3;
	static const string FILE_NAME = "images/cat";
	static const int VERTICAL_SEAM_COUNT = 0;
	static const int HORIZONTAL_SEAM_COUNT = 100;
	static const float MAX_GRADIENT = numeric_limits<float>::infinity();
#pragma endregion


void SetIntensityGradient(const Mat& I, Mat& G)
{
	int m = I.rows,
		n = I.cols;

	Mat Gx(m, n, DataType<float>::type),
		Gy(m, n, DataType<float>::type);

	Sobel(I, Gx, CV_32F, 1, 0, SOBEL_KERNEL);
	Sobel(I, Gy, CV_32F, 0, 1, SOBEL_KERNEL);

	for (int i = 0; i < m; i++)
		for (int j = 0; j < n; j++)
		{
			float gx = Gx.at<float>(i, j);
			float gy = Gy.at<float>(i, j);

			G.at<float>(i, j) = sqrt(gx * gx + gy * gy);
		}
}

void CreateGradientMap(const Mat& I, Mat& G)
{
	Mat greyScale = Mat(I.rows, I.cols, DataType<float>::type);
	cvtColor(I, greyScale, COLOR_BGR2GRAY);
	SetIntensityGradient(greyScale, G);
}


void GrowVerticalSeam(int row, int col, const Mat& gradient, Mat& seam, Mat& directions) 
{
	float grad = gradient.at<float>(row, col);
	if (row == 0 || grad == MAX_GRADIENT)
	{
		seam.at<float>(row, col) = grad;
		return;
	}

	int leftCol = max(col - SEAM_KERNEL, 0);
	int rightCol = min(col + SEAM_KERNEL, gradient.cols - 1);
	int windowWidth = rightCol - leftCol + 1;
	
	Mat window = seam(cv::Rect(leftCol, row - 1, windowWidth, 1));

	double minVal;
	Point minPoint;

	minMaxLoc(window, &minVal, nullptr, &minPoint);

	directions.at<char>(row, col) = minPoint.x - (col - leftCol);
	seam.at<float>(row, col) = grad + minVal;
}

void GrowHorizontalSeam(int row, int col, const Mat& gradient, Mat& seam, Mat& directions)
{
	float grad = gradient.at<float>(row, col);
	if (col == 0 || grad == MAX_GRADIENT)
	{
		seam.at<float>(row, col) = grad;
		return;
	}

	int topRow = max(row - SEAM_KERNEL, 0);
	int bottomRow = min(row + SEAM_KERNEL, gradient.rows - 1);
	int windowHeight = bottomRow - topRow + 1;

	Mat window = seam(cv::Rect(col - 1, topRow, 1, windowHeight));

	double minVal;
	Point minPoint;

	minMaxLoc(window, &minVal, nullptr, &minPoint);

	directions.at<char>(row, col) = minPoint.y - (row - topRow);
	seam.at<float>(row, col) = grad + minVal;
}


//The principal routine to calculate seams
/* @params: src (IN) - image of pixels' energies. In our case - abs(gradient).
			seam (OUT) - seam[i][j] contains the energy for the seam passing from
						the top of src image to pixel (i,j).
			directions (OUT) - contains the shift for the seam passing through the pixel (i, j)
*/
void FindVerticalSeam(const Mat& gradient, Mat& seam, Mat& directions) 
{
	for (int row = 0; row < gradient.rows; ++row) 
		for (int col = 0; col < gradient.cols; ++col)
			GrowVerticalSeam(row, col, gradient, seam, directions);
}

void FindHorizontalSeam(const Mat& gradient, Mat& seam, Mat& directions)
{
	for (int col = 0; col < gradient.cols; ++col)
		for (int row = 0; row < gradient.rows; ++row)
			GrowHorizontalSeam(row, col, gradient, seam, directions);
}


void TraceHorizontalSeam(Mat& gradient, Mat& image, const Mat& seam, const Mat& directions)
{
	Mat lastCol = seam(cv::Rect(seam.cols - 1, 0, 1, seam.rows));
	double minVal;
	Point minPoint;

	minMaxLoc(lastCol, &minVal, nullptr, &minPoint);

	int row = minPoint.y;
	for (int col = gradient.cols - 1; col >= 0; --col)
	{
		image.at<Vec3b>(row, col) = { 0, 0, 255 };
		gradient.at<float>(row, col) = MAX_GRADIENT;
		row += directions.at<char>(row, col);
	}
}

void TraceVerticalSeam(Mat& gradient, Mat& image, const Mat& seam, const Mat& directions)
{
	Mat lastRow = seam(cv::Rect(0, seam.rows - 1, seam.cols, 1));
	double minVal;
	Point minPoint;

	minMaxLoc(lastRow, &minVal, nullptr, &minPoint);

	int col = minPoint.x;
	for (int row = gradient.rows - 1; row >= 0; --row)
	{
		image.at<Vec3b>(row, col) = { 255, 0, 0 };
		gradient.at<float>(row, col) = MAX_GRADIENT;
		col += directions.at<char>(row, col);
	}
}

void RemoveVerticalSeam(Mat& gradient, Mat& image, const Mat& seam, const Mat& directions)
{
	Mat lastRow = seam(cv::Rect(0, seam.rows - 1, seam.cols, 1));
	double minVal;
	Point minPoint;

	minMaxLoc(lastRow, &minVal, nullptr, &minPoint);

	int col = minPoint.x;
	for (int row = gradient.rows - 1; row >= 0; --row)
	{
		image.at<Vec3b>(row, col) = { 255, 0, 0 };
		gradient.at<float>(row, col) = MAX_GRADIENT;
		col += directions.at<char>(row, col);
	}
}


Mat RemoveVerticalSeams(const Mat& gradient, const Mat& image, const int& seams)
{
	int rows = image.rows;
	int cols = image.cols;
	int newCols = cols - seams;

	Mat cropped = Mat(rows, newCols, DataType<Vec3b>::type);

	for (int row = 0; row < rows; ++row)
	{
		int nCol = 0;
		for (int col = 0; col < cols; ++col)
		{
			float grad = gradient.at<float>(row, col);
			if (grad != MAX_GRADIENT)
			{
				cropped.at<Vec3b>(row, nCol) = image.at<Vec3b>(row, col);
				if (nCol < newCols - 1) 
					++nCol;
			}
		}
	}
	return cropped;
}

Mat RemoveHorizontalSeams(const Mat& gradient, const Mat& image, const int& seams)
{
	int rows = image.rows;
	int cols = image.cols;
	int newRows = rows - seams;

	Mat cropped = Mat(newRows, cols, DataType<Vec3b>::type);

	for (int col = 0; col < cols; ++col)
	{
		int nRow = 0;
		for (int row = 0; row < rows; ++row)
		{
			float grad = gradient.at<float>(row, col);
			if (grad != MAX_GRADIENT)
			{
				cropped.at<Vec3b>(nRow, col) = image.at<Vec3b>(row, col);
				if (nRow < newRows - 1) 
					++nRow;
			}
		}
	}
	return cropped;
}

Mat CutVerticalSeams(const Mat& image, const int& seams)
{
	Mat seamsImage = Mat(image.rows, image.cols, DataType<Vec3b>::type),
		gradient = Mat(image.rows, image.cols, DataType<float>::type),
		seam = Mat(image.rows, image.cols, DataType<float>::type),
		directions = Mat(image.rows, image.cols, DataType<char>::type);

	CreateGradientMap(image, gradient);
	image.copyTo(seamsImage);

	for (int i = 0; i < seams; ++i)
	{
		FindVerticalSeam(gradient, seam, directions);
		TraceVerticalSeam(gradient, seamsImage, seam, directions);
	}
	//namedWindow("Vertical Seams", WINDOW_AUTOSIZE);
	//imshow("Vertical Seams", seamsImage);
	imwrite(FILE_NAME + "_VERT_SEAMS= " + to_string(seams) + ".jpg", seamsImage);

	Mat croppedHorizontallyImage = RemoveVerticalSeams(gradient, image, seams);
	return croppedHorizontallyImage;
}

Mat CutHorizontalSeams(const Mat& image, const int& seams)
{
	Mat seamsImage = Mat(image.rows, image.cols, DataType<Vec3b>::type),
		gradient = Mat(image.rows, image.cols, DataType<float>::type),
		seam = Mat(image.rows, image.cols, DataType<float>::type),
		directions = Mat(image.rows, image.cols, DataType<char>::type);

	CreateGradientMap(image, gradient);
	image.copyTo(seamsImage);

	for (int i = 0; i < seams; ++i)
	{
		FindHorizontalSeam(gradient, seam, directions);
		TraceHorizontalSeam(gradient, seamsImage, seam, directions);
	}
	//namedWindow("Horizontal Seams", WINDOW_AUTOSIZE);
	//imshow("Horizontal Seams", seamsImage);
	imwrite(FILE_NAME + "_HOR_SEAMS= " + to_string(seams) + ".jpg", seamsImage);

	Mat croppedVerticallyImage = RemoveHorizontalSeams(gradient, image, seams);
	return croppedVerticallyImage;
}

Mat CropVerHor(const Mat& image, const int& verSeams, const int& horSeams)
{
	return CutVerticalSeams(CutHorizontalSeams(image, horSeams), verSeams);
}

Mat CropHorVer(const Mat& image, const int& verSeams, const int& horSeams)
{
	return CutHorizontalSeams(CutVerticalSeams(image, verSeams), horSeams);
}


int main()
{
	// Loading the image.
	Mat image = imread(FILE_NAME + ".jpg");
	if (!image.data)
		return -1;

	// Showing the initial image.
	//namedWindow("Initial", WINDOW_AUTOSIZE);
	//imshow("Initial", image);

	Mat verHor = CropVerHor(image, VERTICAL_SEAM_COUNT, HORIZONTAL_SEAM_COUNT);
	//namedWindow("Vertical --> Horizontal", WINDOW_AUTOSIZE);
	//imshow("Vertical --> Horizontal", verHor);
	imwrite(FILE_NAME + "_V= " + to_string(VERTICAL_SEAM_COUNT) + "_H= " + to_string(HORIZONTAL_SEAM_COUNT) + ".jpg", verHor);

	
	//Mat horVer = CropHorVer(image, VERTICAL_SEAM_COUNT, HORIZONTAL_SEAM_COUNT);
	//namedWindow("Horizontal --> Vertical", WINDOW_AUTOSIZE);
	//imshow("Horizontal --> Vertical", horVer);
	//imwrite(FILE_NAME + "_H= " + to_string(HORIZONTAL_SEAM_COUNT) + "_H= " + to_string(VERTICAL_SEAM_COUNT), horVer);
	
	//waitKey(0);

	return 0;
}